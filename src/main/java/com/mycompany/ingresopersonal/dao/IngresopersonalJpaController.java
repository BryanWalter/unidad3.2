/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ingresopersonal.dao;

import com.mycompany.ingresopersonal.dao.exceptions.NonexistentEntityException;
import com.mycompany.ingresopersonal.dao.exceptions.PreexistingEntityException;
import com.mycompany.ingresopersonal.entity.Ingresopersonal;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Bryan
 */
public class IngresopersonalJpaController implements Serializable {

    public IngresopersonalJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ingresopersonal ingresopersonal) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ingresopersonal);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findIngresopersonal(ingresopersonal.getRut()) != null) {
                throw new PreexistingEntityException("Ingresopersonal " + ingresopersonal + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingresopersonal ingresopersonal) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ingresopersonal = em.merge(ingresopersonal);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ingresopersonal.getRut();
                if (findIngresopersonal(id) == null) {
                    throw new NonexistentEntityException("The ingresopersonal with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingresopersonal ingresopersonal;
            try {
                ingresopersonal = em.getReference(Ingresopersonal.class, id);
                ingresopersonal.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingresopersonal with id " + id + " no longer exists.", enfe);
            }
            em.remove(ingresopersonal);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingresopersonal> findIngresopersonalEntities() {
        return findIngresopersonalEntities(true, -1, -1);
    }

    public List<Ingresopersonal> findIngresopersonalEntities(int maxResults, int firstResult) {
        return findIngresopersonalEntities(false, maxResults, firstResult);
    }

    private List<Ingresopersonal> findIngresopersonalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingresopersonal.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingresopersonal findIngresopersonal(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingresopersonal.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngresopersonalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingresopersonal> rt = cq.from(Ingresopersonal.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
