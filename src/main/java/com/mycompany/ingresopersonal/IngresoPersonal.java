
package com.mycompany.ingresopersonal;

import com.mycompany.ingresopersonal.dao.IngresopersonalJpaController;
import com.mycompany.ingresopersonal.dao.exceptions.NonexistentEntityException;
import com.mycompany.ingresopersonal.entity.Ingresopersonal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("Retiro")
public class IngresoPersonal {
    
@GET
@Produces(MediaType.APPLICATION_JSON) 
public Response listarSolicitudes(){

    IngresopersonalJpaController dao = new IngresopersonalJpaController();
    List<Ingresopersonal> lista= dao.findIngresopersonalEntities();
    return Response.ok(200).entity(lista).build();

}

@POST
@Produces(MediaType.APPLICATION_JSON)
public Response add(Ingresopersonal ingresopersonal){

    try {
        IngresopersonalJpaController dao = new IngresopersonalJpaController();
        dao.create(ingresopersonal);
    } catch (Exception ex) {
        Logger.getLogger(IngresoPersonal.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok(200).entity(ingresopersonal).build();
    
}

@DELETE
  @Path("/{iddelete}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response delete(@PathParam("iddelete") String iddelete){
  
    try {
        IngresopersonalJpaController dao = new IngresopersonalJpaController();
        dao.destroy(iddelete);
    } catch (NonexistentEntityException ex) {
        Logger.getLogger(IngresoPersonal.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok("Cliente eliminado").build();
  }
  
  @PUT
  public Response update(Ingresopersonal ingresopersonal){
  
    try {
        IngresopersonalJpaController dao = new IngresopersonalJpaController();
        dao.edit(ingresopersonal);
    } catch (Exception ex) {
        Logger.getLogger(IngresoPersonal.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok(200).entity(ingresopersonal).build();
  }


}
