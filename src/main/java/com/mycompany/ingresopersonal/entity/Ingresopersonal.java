/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ingresopersonal.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bryan
 */
@Entity
@Table(name = "ingresopersonal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingresopersonal.findAll", query = "SELECT i FROM Ingresopersonal i"),
    @NamedQuery(name = "Ingresopersonal.findByRut", query = "SELECT i FROM Ingresopersonal i WHERE i.rut = :rut"),
    @NamedQuery(name = "Ingresopersonal.findByNombre", query = "SELECT i FROM Ingresopersonal i WHERE i.nombre = :nombre"),
    @NamedQuery(name = "Ingresopersonal.findByApellido", query = "SELECT i FROM Ingresopersonal i WHERE i.apellido = :apellido"),
    @NamedQuery(name = "Ingresopersonal.findByArea", query = "SELECT i FROM Ingresopersonal i WHERE i.area = :area"),
    @NamedQuery(name = "Ingresopersonal.findByCargo", query = "SELECT i FROM Ingresopersonal i WHERE i.cargo = :cargo")})
public class Ingresopersonal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 2147483647)
    @Column(name = "area")
    private String area;
    @Size(max = 2147483647)
    @Column(name = "cargo")
    private String cargo;

    public Ingresopersonal() {
    }

    public Ingresopersonal(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingresopersonal)) {
            return false;
        }
        Ingresopersonal other = (Ingresopersonal) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.ingresopersonal.entity.Ingresopersonal[ rut=" + rut + " ]";
    }
    
}
